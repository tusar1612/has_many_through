<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    public function course(){
        
        return $this->belongsToMany('App\Course');
    }
}
