<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TeachersTableSeeder::class);
       $this->call(ProfilesTableSeeder::class);
        $this->call(QuartersTableSeeder::class);
         $this->call(CoursesTableSeeder::class);
        $this->call(TrainersTableSeeder::class);
        $this->call(Course_TrainerSeeder::class);
        $this->call(StudentsTableSeeder::class);
    }
}
