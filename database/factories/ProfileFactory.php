<?php

$factory->define(App\Profile::class, function (Faker\Generator $faker) {


    return [
        'bio' => $faker->paragraphs(5,true),
        'web' => $faker->url,
        'facebook' =>'https://www.facebook.com/'.$faker->unique()->firstName,
        'github'=>'https://www.github.com/' . $faker->unique()->lastName,
        'teacher_id' => $faker->numberBetween(1,5)
    ];
});