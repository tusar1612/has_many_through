<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/teachers', function () {

    $teacher_info=\App\Teacher::find(2)->profile;

    return $teacher_info;

});

Route::get('/profiles', function () {

    $teacher_info=\App\Profile::find(2)->teacher;

    return $teacher_info;

});

Route::get('/quarters', function () {

    $quarter=\App\Quarter::find(3)->course;

    return $quarter;

});
Route::get('/trainer', function () {

    $trainer=\App\Trainer::find(4)->course;


        return $trainer;


});
Route::get('/students', function () {

   $students=\App\Quarter::find(3)->students;


    return $students;


});
